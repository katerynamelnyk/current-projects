# Current-projects

## Running locally 

In each folder you will find index.html file, it's enough to drag and drop index.html to your browser window and it will open the website for preview.

## 1_css-skills-flexbox

This project is a simple Landing Page for some brand created using Flexbox.


[screenshot](/uploads/631b79dafe0e01679b63fb1cb466266e/1.png)
___

## 2_css-skills-responsive

This project is a Landing Page for a medicine organization.
__Features:__
- responsive design created with media queries (for desktop, tablet and mobile phone);
- navigation links with a stick position to navigate over the page; 
- hover effect for a block with prices and for buttons;
- some svg icons customized according to design of a page;

[screenshot](/uploads/d8bd4b78a1f1dc7cab78b6062d1eb40d/2.png)
___

## 3_css-skills-bootstrap

This project is a Landing Page about games.
__Features:__
- created using Bootstrap;
- responsive design for desktop, tablet and mobile phone;
- grid layout for main blocks;

[screenshot](/uploads/6f3d62298902b5621501337374880d46/3.png)
___

## 4_css-skills-scss

This project is a Landing Page for a travel agency.
__Features:__
- created using Sass and compiled into CSS via Koala app;
- has 2 style sets: day and night version;
- It's easy to switch into opposite version only by changing URL in index.html;

[screenshot with a day version](/uploads/5115737bf95d1670926ddf112a31cc5a/4_1.png)
[screenshot with a night version](/uploads/7e3fd0b510d33cfa2174d9387febad9d/4_2.png)
___

## 5_js-skills-calculator-jQuery

This project is a simple calculator. 
__Features:__
- can perform basic math operations (add, subtract, multiply, divide);
- show an error when user try to divide by zero;
- log on the right side of a screen info about performed operations;
- user can delete or highlight logged info;
- created using jQuery;

[screenshot](/uploads/a061290a188ead028a87a1320a786faf/5_calculator.jpg)
___

## 6_js-skills-game-rock-paper-scissors

This project is a simple game 'Rock, Paper, Scissors'.
__Features:__
- after pressing one of the buttons, the game is started;
- the result of every step is shown after buttons;
- after three wins or three losses, the final result is shown;
- reset button clears game data and previous results;

[screenshot](/uploads/66dad97672848302436f2dcb1ac56c5c/6_game.jpg)
___

## 7_js-skills-simple-twitter-web-storage-API

This project is a simple 'Twitter - like' application.
__Features:__
- add new tweet;
- edit existing tweet;
- mark tweet as liked;
- unlike tweets;
- remove tweet from list;
- show error alert;
- show liked alert;

__Application consists of four pages:__
- main page;
- add tweet;
- liked tweets;
- edit appropriate tweet;

[screenshot](/uploads/211d9a383dc2b57dfa318f2ccb8763c2/7_twitter.jpg)
___


