const root = document.getElementById('root');
let addTweetButton = root.querySelector('.addTweet');
let modifyItem = root.querySelector('#modifyItem');
let saveModifiedItemButton = root.querySelector('#saveModifiedItem');
let cancelModification = root.querySelector('#cancelModification');
let modifyItemInput = root.querySelector('#modifyItemInput');
modifyItemInput.setAttribute('rows', '10');
modifyItemInput.setAttribute('cols', '50');
let listOfTweets = root.querySelector('#list');
let navigationButtons = root.querySelector('#navigationButtons');
let alertMessage = root.querySelector('#alertMessage');
let alertMessageText = root.querySelector('#alertMessageText');
const millisecondsToHideAlert = 2000;


document.addEventListener('DOMContentLoaded', function () {
    showTweets();
});

addTweetButton.addEventListener('click', function () {
    listOfTweets.classList.toggle('hidden');
    modifyItem.classList.toggle('hidden');
    modifyItemInput.value = '';
    history.pushState(null, null, 'add');
    window.addEventListener('popstate', function (e) {
        console.log(e.state);
    })
})

cancelModification.addEventListener('click', function () {
    modifyItem.classList.toggle('hidden');
    listOfTweets.classList.toggle('hidden');
    history.back();
})

saveModifiedItemButton.addEventListener('click', function () {
    let currentKey = 0;
    let keys = Object.keys(localStorage);
    keys = Array.from(keys)
    for (let i = 0; i < keys.length; i++) {
        keys[i] = Number(keys[i]);
    }
    for (let i = 0; i < keys.length; i++) {
        if (keys[i] > +currentKey) {
            currentKey = keys[i];
        }
    }
    currentKey++;
    let tweetText = modifyItemInput.value;
    let checkedLength = checkTweetLength(tweetText);
    let checkedDuplicate = checkTweetDuplicate(tweetText);
    if (checkedLength && checkedDuplicate) {
        localStorage.setItem(currentKey, tweetText);
        createTweet(currentKey);
        modifyItem.classList.toggle('hidden');
        listOfTweets.classList.toggle('hidden');
        history.back();
    } else if (checkedDuplicate === false) {
        showAlertError();
        setTimeout(hideAlert, millisecondsToHideAlert);
    }
})

function showTweets() {
    let keys = Object.keys(localStorage);
    for (let key of keys) {
        createTweet(key);
    }
}

function createRemoveButton(li) {
    let removeButton = document.createElement('input');
    removeButton.setAttribute('type', 'button')
    removeButton.setAttribute('value', 'remove')
    removeButton.classList.add('removeTweetButton');
    li.append(removeButton);
    removeButton.onclick = function () {
        let currentLI = this.parentElement;
        let value = currentLI.firstElementChild.textContent;
        for (let key of Object.keys(localStorage)) {
            if (localStorage.getItem(key) === value) {
                localStorage.removeItem(key);
            }
        }
        currentLI.remove();
    }
}

function createLikeButton(li) {
    let likeButton = document.createElement('input');
    likeButton.setAttribute('type', 'button')
    likeButton.setAttribute('value', 'like')
    likeButton.setAttribute('name', 'like')
    likeButton.classList.add('likeTweetButton');
    li.append(likeButton);
    let like = document.createElement('span');
    like.classList.add('like');
    like.innerHTML = '&#10084;';
    like.classList.toggle('unvisible');
    li.append(like);
    likeButton.onclick = function () {
        if (likeButton.value === 'like') {
            likeButton.value = 'unlike';
            like.classList.toggle('unvisible');
            li.classList.add('liked');
            addTweetButton.after(goToLikedButton);
            let currentKey;
            let currentLI = this.parentElement;
            let value = currentLI.firstElementChild.textContent;
            for (let key of Object.keys(localStorage)) {
                if (localStorage.getItem(key) === value) {
                    currentKey = key;
                }
            }
            showAlertLiked(currentKey);
            setTimeout(hideAlert, millisecondsToHideAlert);
        } else if (likeButton.value === 'unlike') {
            likeButton.value = 'like';
            like.classList.toggle('unvisible');
            li.classList.remove('liked');
            let likedButtons = document.getElementsByName('like');
            let isLiked = false;
            for (let but of likedButtons) {
                if (but.defaultValue === 'unlike') {
                    isLiked = true;
                }
            }
            if (isLiked === false) {
                goToLikedButton.remove();
            }
            let currentKey;
            let currentLI = this.parentElement;
            let value = currentLI.firstElementChild.textContent;
            for (let key of Object.keys(localStorage)) {
                if (localStorage.getItem(key) === value) {
                    currentKey = key;
                }
            }
            showAlertUnLiked(currentKey);
            setTimeout(hideAlert, millisecondsToHideAlert);
        }
    }

}


function createTweet(key) {
    let li = document.createElement('li');
    let spanWithText = document.createElement('span');
    li.append(spanWithText);
    spanWithText.textContent = localStorage.getItem(key);
    listOfTweets.append(li);
    createRemoveButton(li);
    createLikeButton(li);
    spanWithText.onclick = function () {
        listOfTweets.classList.toggle('hidden');

        let editionDiv = document.createElement('div');
        editionDiv.setAttribute('id', 'edition');

        let textAreaToEdit = document.createElement('textarea');
        textAreaToEdit.setAttribute('id', 'textAreaToEdit');
        textAreaToEdit.setAttribute('rows', '10');
        textAreaToEdit.setAttribute('cols', '50');

        let editButtons = document.createElement('div');
        editButtons.setAttribute('id', 'editButtons');

        let submitEditButton = document.createElement('input');
        submitEditButton.setAttribute('id', 'submitEditButton');
        submitEditButton.setAttribute('value', 'Save Changes');
        submitEditButton.setAttribute('type', 'button');

        let cancelEditButton = document.createElement('input');
        cancelEditButton.setAttribute('id', 'cancelEditButton');
        cancelEditButton.setAttribute('value', 'Cancel');
        cancelEditButton.setAttribute('type', 'button');

        editionDiv.append(textAreaToEdit);
        editButtons.append(cancelEditButton);
        editButtons.append(submitEditButton);
        editionDiv.append(editButtons);
        root.append(editionDiv);

        textAreaToEdit.textContent = spanWithText.textContent;

        history.pushState(null, null, `edit/:item_${key}`);
        window.addEventListener('popstate', function (e) {
            console.log(e.state);
        })

        cancelEditButton.addEventListener('click', function () {
            listOfTweets.classList.toggle('hidden');
            editionDiv.classList.toggle('hidden');
            history.back();
        })

        submitEditButton.addEventListener('click', function () {
            let newTweetText = textAreaToEdit.value;
            let checkedLength = checkTweetLength(newTweetText);
            let checkedDuplicate = checkTweetDuplicate(newTweetText);
            if (checkedLength && checkedDuplicate) {
                localStorage.setItem(key, newTweetText);
                spanWithText.textContent = newTweetText;
                listOfTweets.classList.toggle('hidden');
                editionDiv.classList.toggle('hidden');
                history.back();
            } else if (checkedDuplicate === false) {
                showAlertError();
                setTimeout(hideAlert, millisecondsToHideAlert);
            }
        })
    }
}

let goToLikedButton = document.createElement('input');
goToLikedButton.setAttribute('type', 'button');
goToLikedButton.setAttribute('value', 'Go to liked');

let backFromLikedButtons = document.createElement('input');
backFromLikedButtons.setAttribute('type', 'button');
backFromLikedButtons.setAttribute('value', 'back');
backFromLikedButtons.classList.add('hidden');
navigationButtons.append(backFromLikedButtons);

backFromLikedButtons.onclick = function () {
    history.back();
    addTweetButton.classList.toggle('hidden');
    goToLikedButton.classList.toggle('hidden');
    backFromLikedButtons.classList.toggle('hidden');
    let allTweets = document.getElementsByTagName('li');
    for (let tweet of allTweets) {
        tweet.classList.remove('hidden');
    }
}

goToLikedButton.onclick = function () {
    history.pushState(null, null, 'liked');
    window.addEventListener('popstate', function (e) {
        console.log(e.state);
    })
    addTweetButton.classList.toggle('hidden');
    goToLikedButton.classList.toggle('hidden');
    backFromLikedButtons.classList.toggle('hidden');
    let allTweets = document.getElementsByTagName('li')
    for (let tweet of allTweets) {
        if (!tweet.classList.contains('liked')) {
            tweet.classList.add('hidden');
        }
    }
}

function checkTweetLength(tweetText) {
    let checked = false;
    let maxLengthOfTweet = 140;
    if (tweetText.length <= maxLengthOfTweet && tweetText.length >= 1) {
        checked = true;
    }
    return checked;
}

function checkTweetDuplicate(tweetText) {
    let checked = true;
    let keys = Object.keys(localStorage);
    for (let key of keys) {
        if (localStorage.getItem(key) === tweetText) {
            checked = false;
        }
    }
    return checked;
}

function showAlertError() {
    alertMessage.classList.toggle('hidden');
    alertMessageText.textContent = 'Error! You can\'t tweet about that.';
}

function hideAlert() {
    alertMessage.classList.toggle('hidden');
}

function showAlertLiked(id) {
    alertMessage.classList.toggle('hidden');
    alertMessageText.textContent = `Hooray! You liked tweet with id ${id}!`;
}

function showAlertUnLiked(id) {
    alertMessage.classList.toggle('hidden');
    alertMessageText.textContent = `Sorry! You no longer like tweet with id ${id}.`;
}

function findKey() {
    let currentKey;
    let currentLI = this.parentElement;
    let value = currentLI.firstElementChild.textContent;
    for (let key of Object.keys(localStorage)) {
        if (localStorage.getItem(key) === value) {
            currentKey = key;
        }
    }
    return currentKey;
}