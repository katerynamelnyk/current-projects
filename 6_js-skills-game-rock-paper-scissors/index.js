const resetBut = document.querySelector('.reset');
const amountOfWinsOrLoses = 3;

let game = {
    round: 0,
    win: 0,
    lose: 0
}

let gameButtons = document.getElementsByClassName('game-button');
gameButtons = Array.from(gameButtons);

gameButtons.forEach(element => {
    element.addEventListener('click', function (event) {
        game.round += 1;
        let currentGamerResult = event.currentTarget.value;
        let currentMachineResult = gameValues[getRandomGameValue()];
        let currentResult = getAResult(currentGamerResult, currentMachineResult);

        if (currentResult === 'WON') {
            game.win += 1;
        } else if (currentResult === 'LOST') {
            game.lose += 1;
        }

        renderABlockWithResult(currentGamerResult, currentMachineResult, currentResult, game.round);
        if (game.win === amountOfWinsOrLoses) {
            renderFinalWinBlock();
            clearGameData();
        }
        if (game.lose === amountOfWinsOrLoses) {
            renderFinalLoseBlock();
            clearGameData()
        }
    })
});

resetBut.addEventListener('click', function () {
    clearGameData();
    resultSection.textContent = '';
})

let clearGameData = () => {
    Object.keys(game).forEach(key => {
        game[key] = 0;
    });
}

const resultSection = document.querySelector('.result-section');

let renderABlockWithResult = (gamerResult, machineResult, result, round) => {
    let div = document.createElement('div');
    let content = '';
    if (gamerResult === machineResult) {
        content = `Round ${round}, ${gamerResult} vs. ${machineResult}, ${result}!`;
    } else {
        content = `Round ${round}, ${gamerResult} vs. ${machineResult}, You've ${result}!`;
    }
    div.textContent = content;
    div.classList.add('result');
    resultSection.appendChild(div);
}

let renderFinalWinBlock = () => {
    let div = document.createElement('div');
    div.classList.add('winner');
    let content = 'Congratulations! You are the winner!';
    div.textContent = content;
    resultSection.appendChild(div);
}
let renderFinalLoseBlock = () => {
    let div = document.createElement('div');
    div.classList.add('looser');
    let content = 'Sorry! May be next time!'
    div.textContent = content;
    resultSection.appendChild(div);
}

const gameValues = ['Rock', 'Paper', 'Scissors'];

let getRandomGameValue = () => {
    return Math.floor(Math.random() * gameValues.length);
}

let getAResult = (gamerResult, machineResult) => {
    let result = '';
    if (gamerResult === 'Scissors') {
        if (machineResult === 'Paper') {
            result = 'WON';
        } else if (machineResult === 'Rock') {
            result = 'LOST';
        } else {
            result = 'Try again';
        }
    }
    if (gamerResult === 'Paper') {
        if (machineResult === 'Rock') {
            result = 'WON';
        } else if (machineResult === 'Scissors') {
            result = 'LOST';
        } else {
            result = 'Try again';
        }
    }
    if (gamerResult === 'Rock') {
        if (machineResult === 'Scissors') {
            result = 'WON';
        } else if (machineResult === 'Paper') {
            result = 'LOST';
        } else {
            result = 'Try again';
        }
    }
    return result;
}