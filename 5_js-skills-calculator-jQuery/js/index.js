const screenBlock = document.querySelector('.calculator-screen_block');
const operators = ['+', '-', '*', '/'];

$('button:not(.empty)').click(function (event) {
    let val = event.currentTarget.value;
    let length = screenBlock.textContent.length;

    if (screenBlock.textContent === '0') {
        screenBlock.textContent = val;
    } else if (operators.includes(screenBlock.textContent[length - 1]) && operators.includes(val)) {
        let tmpVal = screenBlock.textContent;
        let newVal = '';
        for (let i = 0; i < tmpVal.length - 1; i++) {
            newVal += tmpVal[i];
        }
        newVal += val;
        screenBlock.textContent = newVal;
    } else if (screenBlock.textContent[length - 1] === '/' && val === '0') {
        screenBlock.textContent = 'ERROR';
        screenBlock.style.color = 'red';
    } else if (screenBlock.textContent === 'ERROR') {
        screenBlock.style.color = 'black';
        screenBlock.textContent = val;
    } else {
        screenBlock.textContent += val;
    }
})

$('.clear-operator').click(function () {
    screenBlock.textContent = '0';
})

$('.result').click(function () {
    let value = screenBlock.textContent;
    let arrOfNum = [];
    let arrOfOperators = [];
    let result = 0;
    let currentNumber = '';

    if (value !== '') {
        for (let i = 0; i < value.length; i++) {
            if (isFinite(value[i])) {
                currentNumber += value[i];
                if (operators.includes(value[i + 1]) || value[i + 1] === undefined) {
                    arrOfNum.push(+currentNumber);
                    currentNumber = '';
                }
            } else if (operators.includes(value[i])) {
                arrOfOperators.push(value[i]);
            }
        }
        let ind = 0;
        arrOfNum.reduce((previous, current) => {
            if (arrOfOperators[ind] === '+') {
                result = previous + current;
            }
            if (arrOfOperators[ind] === '-') {
                result = previous - current;
            }
            if (arrOfOperators[ind] === '*') {
                result = previous * current;
            }
            if (arrOfOperators[ind] === '/') {
                result = previous / current;
            }
            ind++;
            return result;
        })

        screenBlock.textContent = result;

        createLogBlock(value, result);

    }
})

$('.logs').scroll(function (event) {
    console.log(`Scroll Top: ${event.target.scrollTop}`);
})

function createLogBlock(value, result) {
    let logBlock = document.createElement('div');
    logBlock.classList.add('log-block');

    let circle = document.createElement('div');
    circle.classList.add('circle');
    logBlock.appendChild(circle);

    $('.circle').click(function () {
        $(this).toggleClass('red');
    })
    $(document).ready(function () {
        $('.circle').click(function () {
            $(this).toggleClass('red');
        })
    })

    let equation = document.createElement('div');
    equation.classList.add('equation');
    let content = `${value}=${result}`;
    equation.textContent = content;

    for (let i = 0; i < content.length; i++) {
        if (content[i] === '4' && content[i + 1] === '8') {
            equation.classList.add('special');
        }
    }
    $(document).ready(function () {
        $('.equation').filter(function () {
            return $(this).hasClass('special');
        })
            .css('text-decoration', 'underline')
    })

    logBlock.appendChild(equation);

    let close = document.createElement('div');
    close.classList.add('close');
    logBlock.appendChild(close);

    $(document).ready(function () {
        $('.close').click(function () {
            let currentLogBlock = $(this).parent();
            currentLogBlock.remove();
        })
    })
    $(document).ready(function () {
        $('.close').mouseenter(function () {
            $(this).css('color', 'red');
        })
    })
    $(document).ready(function () {
        $('.close').mouseleave(function () {
            $(this).css('color', 'black');
        })
    })

    $('.logs').prepend(logBlock);
}





